import java.util.Scanner;

public class PartThree {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Input the length of the square.");
		double lengthSquare = sc.nextDouble();
		System.out.println("Now input the length and the width of the rectangle.");
		double lengthRectangle = sc.nextDouble();
		double widthRectangle = sc.nextDouble();
		
		double squareArea = AreaComputations.areaSquare(lengthSquare);
		AreaComputations ac = new AreaComputations();
		double rectangleArea = ac.areaRectangle(lengthRectangle,widthRectangle);
		
		System.out.println("The square's area is : " + squareArea);
		System.out.println("The rectangle's area is : " + rectangleArea);
	}
}