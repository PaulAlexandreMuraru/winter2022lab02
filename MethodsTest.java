public class MethodsTest {
	public static void main(String[] args) {
		int x = 10;
		System.out.println(x);
		methodNoInputNoReturn();
		System.out.println(x);
		methodOneInputNoReturn(10);
		methodOneInputNoReturn(x);
		methodOneInputNoReturn(x+50);
		methodTwoInputNoReturn(1, 1.0);
		System.out.println(methodNoInputReturnInt());
		double z = sumSquareRoot(3,6);
		System.out.println(z);
		
		String s1 = "hello";
		String s2 = "goodbye";
		System.out.println(s1.length());
		System.out.println(s2.length());
		
		SecondClass sc = new SecondClass();
		
		System.out.println(SecondClass.addOne(50));
		System.out.println(sc.addTwo(50));
	}
	
	public static void methodNoInputNoReturn() {
		int x = 50;
		System.out.println("I'm in a method that takes no input and returns nothing");
	}
	
	public static void methodOneInputNoReturn(int unknownValue) {
		System.out.println("Inside the method one input no return " + unknownValue);
	}
	
	public static void methodTwoInputNoReturn(int val_i, double val_d) {
		System.out.println(val_i+val_d);
	}
	
	public static int methodNoInputReturnInt() {
		return 6;
	}
	
	public static double sumSquareRoot(int val_1, int val_2) {
		double sum = val_1+val_2;
		double sqrt = Math.sqrt(sum);
		return sqrt;
	}
}